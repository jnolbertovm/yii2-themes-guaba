<div class="gt-header gt-header--fixed">
    <nav class="gt-header__nav">
        <a href="#" class="gt-header__logo">Guaba</a>
    </nav>
    <nav class="nav gt-header__nav">
        <a href="" class="gt-header__link">
            <i class="fas fa-th"></i>
        </a>
        <a href="" class="gt-header__link">
            <img class="rounded-circle" src="https://via.placeholder.com/32" width="32" alt="">
        </a>
    </nav>
</div>