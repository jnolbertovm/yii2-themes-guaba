<aside class="gt-sidebar gt-sidebar--fixed">
    <div class="gt-sidebar__header">
        <div class="gt-sidebar__logo">Guaba</div>
    </div>
    <div class="gt-sidebar__body"></div>
    <a class="gt-sidebar__toggle" type="button">
        <i class="fas fa-angle-double-left"></i>
        <i class="fas fa-angle-double-right"></i>
        <span>
            Contraer la barra lateral
        </span>
    </a>
</aside>