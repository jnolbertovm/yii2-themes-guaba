<?php

use yii\helpers\Html;

if (class_exists('app\assets\AppAsset')) {
    app\assets\AppAsset::register($this);
}

jnvm\yii2\themes\guaba\web\Assets::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/jnolbertovm/guaba/assets');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
            <?= $this->render('header.php', compact("directoryAsset")) ?>
            <?= $this->render('sidebar.php', compact("directoryAsset")) ?>
            <div class="gt-content">
                <?= $content ?>
            </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
