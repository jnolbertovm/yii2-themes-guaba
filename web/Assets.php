<?php

namespace jnvm\yii2\themes\guaba\web;

use yii\web\AssetBundle;

/**
 * Guaba AssetBundle
 * @since 0.1
 */
class Assets extends AssetBundle
{
    public $sourcePath = '@vendor/jnolbertovm/guaba/assets';
    
    public $css = [
        'css/guaba.min.css',
    ];

    public $skin = 'default';

    // public $depends = [
    //     'yii\web\YiiAsset'
    // ];
    
    // public function init()
    // {
    //     // Append skin color file if specified
    //     if ($this->skin) {
    //         if (('default' !== $this->skin) && (strpos($this->skin, 'skin-') !== 0)) {
    //             throw new Exception('Invalid skin specified');
    //         }
    //         $this->css = [sprintf('css/skins/guaba-%s.min.css', $this->skin)];
    //     }
    //     parent::init();
    // }
}
